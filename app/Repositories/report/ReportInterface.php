<?php
namespace App\Repositories\report;

interface  ReportInterface
{
    /**
     * Get 5 posts hot in a month the last
     * @return mixed
     */
    public function getPostHost();
    public function getErorsWithPaginate($perPage, $search = null, $status = null);
}
