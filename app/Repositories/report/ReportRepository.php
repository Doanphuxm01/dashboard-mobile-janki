<?php
namespace App\Repositories\report;

use App\Repositories\EloquentRepository;
use Illuminate\Support\Carbon;
use App\Models\JankiReport;

class ReportRepository extends EloquentRepository implements  ReportInterface
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return JankiReport::class;
    }
    /**
     * Get 5 posts hot in a month the last
     * @return show
     */
    public function getPostHost()
    {
        return $this->_model::where('created_at', '>=', Carbon::now()->subMonth())->orderBy('view', 'desc')->take(5)->get();
    }
    /**
     * Get 5 posts hot in a month the last
     * @return Paginate
     */
    public function getErorsWithPaginate($perPage, $search = null, $status = null){
        $query = JankiReport::query();

        if ($status) {
            $query->where('status', $status);
        }

        if ($search) {
            $query->where(function ($q) use ($search) {
                $q->where('content', "like", "%{$search}%");
            });
        }

        $result = $query->orderBy('id', 'desc')
            ->paginate($perPage);

        if ($search) {
            $result->appends(['search' => $search]);
        }

        if ($status) {
            $result->appends(['status' => $status]);
        }

        return $result;
    }
}
?>
