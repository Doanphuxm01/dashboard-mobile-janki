<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleOff extends Model
{
    protected $table = 'saleoff';
    protected $guarded = ['id'];
}

public function updateAdver($country){
        $update = SaleOff::where('country', $country['bonus'])->update([
            'title_ios' => $country['title_ios'],
            'title_android' => $country['title_android'],
            'description_ios' => $country['description_ios'],
            'description_android'=>$country['description_android'],
            'link_ios' => $country['link_ios'],
            'link_android' => $country['link_android'],
            'sale_ios' => $country['sale_ios'],
            'sale_android'=>$country['sale_android'],
            'button_ios' => $country['button_ios'],
            'button_android' => $country['button_android'],
            'active' => $country['active'],
            'start_android'=>$country['start_android'],
            'end_android'=>$country['end_android'],
            'start_ios'=>$country['start_ios'],
            'end_ios'=>$country['end_ios'],
            'cooldown_ios'=>$country['cooldown_ios'],
            'cooldown_android'=>$country['cooldown_android']
        ]);
        return $update;
    }

    public function createAdver($country){
        $exists = SaleOff::where('country', $country['add-country'])->count();

        if($exists >0 ){
            return 0;
        }else{
            $insert = SaleOff::insert(array(
                'country'=> $country['add-country'],
                'title_ios' => $country['title_ios'],
                'title_android' => $country['title_android'],
                'description_ios' => $country['description_ios'],
                'description_android'=>$country['description_android'],
                'link_ios' => $country['link_ios'],
                'link_android' => $country['link_android'],
                'sale_ios' => $country['sale_ios'],
                'sale_android'=>$country['sale_android'],
                'button_ios' => $country['button_ios'],
                'button_android' => $country['button_android'],
                'active' => $country['active'],
                'start_android'=>$country['start_android'],
                'end_android'=>$country['end_android'],
                'start_ios'=>$country['start_ios'],
                'end_ios'=>$country['end_ios'],
                'cooldown_ios'=>$country['cooldown_ios'],
                'cooldown_android'=>$country['cooldown_android']
                ));
            return 1;
        }
   
    }