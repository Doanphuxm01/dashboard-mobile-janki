<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Validator;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string v 
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function users(){
        return 'users';
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login (Request $request){
        $rules = [
            'email' =>'required|email',
            'password' => 'required|min:6'
        ];
        $messages = [
            'email.required' => 'Email là trường bắt buộc',
            'email.email' => 'Email không đúng định dạng',
            'password.required' => 'Mật khẩu là trường bắt buộc',
            'password.min' => 'Mật khẩu phải chứa ít nhất 8 ký tự',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect()->route('admin.login')->withErrors($validator)->withInput();
        } else {
            $email = $request->input('email');
            $password = $request->input('password');

            if( Auth::attempt(['email' => $email, 'password' =>$password])) {
                return redirect()->route('admin.dashboard');
            } else {
                Session::flash('error', 'Email hoặc mật khẩu không đúng!');
                return redirect()->route('admin.login');
            }
        }
    }
    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
    public function register(Request $request){
        if($request->isMethod('POST')){
            $user = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'admin'    => 1,
                'role'     => 0,
                'status'   =>0,
            ];
            $registration = User::insert($user);
            return redirect()->route('admin.login');
        }
        $view = view('auth.register');
        return $view;
    }
}
