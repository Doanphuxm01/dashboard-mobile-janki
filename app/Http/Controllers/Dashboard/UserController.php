<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class UserController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function ListUser( Request $request){
        $all = $request->all();
        if(isset($all['id'])){
            $id = $all['id'];
            unset($all['id']);
            $this->user->where('id', $id)->update($all);
        }
        $users = $this->user->where('admin', 1)->paginate(12);
        $view = view('backend.users.index');
        $view->with('users', $users);
        return $view;
    }
}
