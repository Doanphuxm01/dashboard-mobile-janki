<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\report\ReportRepository;
use App\Models\JankiReport;
use Illuminate\Support\Facades\Input;
class Janki_QuestionError extends Controller
{
   private $report;

   public function __construct( ReportRepository $report){
        $this->report = $report;
    }
    public function index(){
        $report = $this->report->getAll();
        $report   = $this->report->getErorsWithPaginate(
            $perPage = 5,
            Input::get('search'),
            Input::get('status')
        );
        $view = view('backend.janki.report.index');
        $view->with('report',$report);
        return $view;
    }
}
