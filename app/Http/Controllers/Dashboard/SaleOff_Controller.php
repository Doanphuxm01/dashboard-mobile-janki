<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Models\SaleOff;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaleOffRequest;
class SaleOff_Controller extends Controller
{
    private $saleOff;

    public function __construct()
    {
        $this->saleOff = new SaleOff();
    }
    public  function index(Request $request){
        $country = $request->country;
        if(empty($country)){
            $data = $this->saleOff->first();
        }else{
            $data = $this->saleOff->where('country', $country)->first();
            // dd($data);
        }
        $list = $this->saleOff->get(['country']);
        if ($data !== null) {
            $data->sale_ios = json_decode($data->sale_ios);
            $data->sale_android = json_decode($data->sale_android);
        }
        $data['start_android'] = str_replace(' ', 'T', $data['start_android']);
        $data['end_android'] = str_replace(' ', 'T', $data['end_android']);
        $data['start_ios'] = str_replace(' ', 'T', $data['start_ios']);
        $data['end_ios'] = str_replace(' ', 'T', $data['end_ios']);
        $view = view('backend.saleOff.index');
        $view->with('data', $data);
        $view->with('list', $list);
        $view->with('country', $country);
        return $view;
    }

    public function editSale(SaleOffRequest $request){
        $country = $request->all();
        $country['sale_ios'] = [];
        $country['sale_android'] = [];
        $country['sale_ios'] = array(
            0 => array
            (
                "premium" => "pre6months",
                "percent" => $country['pre-6-ios']
            ),
            1 => array
            (
                "premium" => "preforevermonths",
                "percent" => $country['pre-fore-ios']
            ));


        $country['sale_android'] = array(
            0 => array
            (
                "premium" => "pre6months",
                "percent" => $country['pre-6-android']
            ),
            1 => array
            (
                "premium" => "preforevermonths",
                "percent" => $country['pre-fore-android']
            ));
        $country['sale_ios'] = json_encode($country['sale_ios']);
        $country['sale_android'] = json_encode($country['sale_android']);
        if ($country['country'] !== '' && $country['country'] !== null) {
            $create = $this->saleOff->createAdver($country);
            if ($create == 1) {
                $mess = 'Thêm thành công.';

                return redirect()->back()->with('mess', $mess);
            }
            else{
                $mess = 'Tên nước đã có.';

                return redirect()->back()->with('mess', $mess);
            }
        }else{
            // dd($country);
            if (isset($country['bonus'])) {
                $update= $this->saleOff->updateAdver($country);
                return back();
            }else{
                $mess = 'Chưa có quốc gia';
                return redirect()->back()->with('mess', $mess);
            }

        }
    }
}
