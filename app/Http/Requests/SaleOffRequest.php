<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaleOffRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_ios'               => 'required',
            'title_android'           => 'required',
            'description_ios'         => 'required',
            'description_android'     => 'required',
            'link_ios'                => 'required',
            'link_android'            => 'required',
            'pre-6-android'           => 'required',
            'pre-fore-android'        => 'required',
            'pre-6-ios'               => 'required',
            'pre-fore-ios'            => 'required',
            'start_android'           => 'required',
            'end_android'             => 'required',
            'start_ios'               => 'required',
            'end_ios'                 => 'required',
            'button_ios'              => 'required',
            'button_android'          => 'required',
            'cooldown_android'        => 'required',
            'cooldown_ios'            => 'required',
            'country'                 => 'unique:saleoff,country',
        ];
    }
}
