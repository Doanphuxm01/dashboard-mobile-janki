<?php

use Illuminate\Database\Seeder;
use App\Models\JankiReport;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // factory(JankiReport::class,30)->create();
       DB::table('users')->insert([
           'password' => bcrypt('admin'),
           'name' => 'admin',
           'email' => 'admin@admin.net',
           'role'  => 1,
           'status' => 1 ,
           'admin'  => 1
       ]);
    }
}
