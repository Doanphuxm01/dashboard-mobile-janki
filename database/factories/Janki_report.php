<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Carbon\Carbon;
use Faker\Generator as Faker;
use App\Models\JankiReport;
$factory->define(JankiReport::class, function (Faker $faker) {
    $date = Carbon::now()->modify('-2 year');
    $createdDate = clone($date);
    return [
        'content' => $faker->content,
        'status' => $faker->status,
        'created_at' => $createdDate,
        'updated_at' => $createdDate
    ];
});
