<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
    })->name('admin.login');
Route::group([
    'namespace' => 'Auth'
], function(){
    Route::match(['get', 'post'], 'login', 'LoginController@login')->name('admin.post.login');

    Route::match(['get', 'post'], 'logout', 'LoginController@logout')->name('admin.logout');
    Route::match(['get', 'post'], 'register', 'LoginController@register')->name('admin.register');
});
Route::group([
    'middleware' => 'auth',
    'prefix' => 'admin',
    'namespace' =>'dashboard',
], function () {
   Route::get('','HomeController@myHome')->name('admin.dashboard');
   Route::get('report','Janki_QuestionError@index')->name('janki.index');

   Route::group(['prefix' => 'users'], function () {
       Route::any('/','UserController@ListUser')->name('admin.users');
   });

   Route::group([
       'middleware' => 'auth',
       'prefix' => 'sale',
   ],function(){
       Route::any('','SaleOff_Controller@index')->name('admin.sale');
       Route::any('edit','SaleOff_Controller@editSale')->name('admin.sale.edit');
   });
});

