@extends('backend.layouts.master')
@section('title')
    users
@endsection
@section('contents')
<div class="table-responsive">
    <div class="col-sm-12 box-content m-t-10">
    <table class="table table-striped table-bordered table-hover jambo_table bulk_action" id="dataTable" width="100%" cellspacing="0">
      <thead>
        <tr>
            <th>STT</th>
            <th>Username</th>
            <th>Email</th>
            <th>Quyền</th>
            <th>Trạng Thái</th>
        </tr>
      </thead>
      <tbody>
        @foreach($users as $key => $user)
        {{-- {{ dd($users->toArray()) }} --}}
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>
                <select class="form-control fillter_width_url" name="role">
                    <option value="{{ route('admin.users', ['id' => $user->id, 'role' => 0]) }}" {{ ($user->role == 0) ? 'selected' : '' }}>User</option>
                    <option value="{{ route('admin.users', ['id' => $user->id, 'role' => 1]) }}" {{ ($user->role == 1) ? 'selected' : '' }}>Admin</option>
                    <option value="{{ route('admin.users', ['id' => $user->id, 'role' => 2]) }}" {{ ($user->role == 2) ? 'selected' : '' }}>CTV</option>
                </select>
            </td>
            <td>   
                <select class="form-control fillter_width_url" name="status">
                    <option value="{{ route('admin.users', ['id' => $user->id, 'status' => 0]) }}" {{ ($user->status) ? '' : 'selected' }}>Deactive</option>
                    <option value="{{  route('admin.users', ['id' => $user->id, 'status' => 1]) }}" {{ ($user->status) ? 'selected' : '' }}>Active</option>
                </select>
            </td>
        </tr>
          @endforeach
      </tbody>
    </table>
    @include('backend.dashboard.includes.pagination', ['data' => $users, 'appended' => ['search' => Request::get('search')]])
  </div>
</div>
@endsection
