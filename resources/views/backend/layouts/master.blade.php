<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('title')admin</title>

  <!-- Custom fonts for this template-->
<link href="{{asset('dashboard/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('dashboard/font/css.css')}}" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset('dashboard/css/sb-admin-2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('dashboard/css/customs.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/assets/libs/flatpickr/flatpickr.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- App css -->
{{--    <link href="{{ asset('dashboard/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
{{--    <link href="{{ asset('dashboard/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />--}}
{{--    <link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" />--}}
  <meta name="csrf-token" content="{{ csrf_token() }}" />
        {{-- after css --}}
    @yield('after-css')

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    @include('backend.dashboard.includes.menu')
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        @include('backend.dashboard.includes.nav')
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
            @yield('contents')
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      <!-- Footer -->
      @include('backend.dashboard.includes.footer')
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ route('admin.logout') }}">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
<script src="{{asset('dashboard/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('dashboard/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
<script src="{{asset('dashboard/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
<script src="{{asset('dashboard/js/sb-admin-2.min.js')}}"></script>

  <!-- Page level plugins -->
{{--<script src="{{asset('dashboard/vendor/chart.js/Chart.min.js')}}"></script>--}}

  <!-- Page level custom scripts -->
{{--<script src="{{asset('dashboard/js/demo/chart-area-demo.js')}}"></script>--}}
{{--<script src="{{asset('dashboard/js/demo/chart-pie-demo.js')}}"></script>--}}
<script src="{{ asset('dashboard/js/backend/fillterUrl.js')}}"></script>
  <!-- Plugins js-->
  <script src="{{ asset('dashboard/assets/libs/flatpickr/flatpickr.min.js') }}"></script>
  <script src="{{ asset('dashboard/assets/libs/jquery-knob/jquery.knob.min.js') }}"></script>
  <script src="{{ asset('dashboard/assets/libs/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
{{--  <script src="{{ asset('dashboard/assets/libs/flot-charts/jquery.flot.js') }}"></script>--}}
{{--  <script src="{{ asset('dashboard/assets/libs/flot-charts/jquery.flot.resize.js') }}"></script>--}}
{{--  <script src="{{ asset('dashboard/assets/libs/flot-charts/jquery.flot.time.js') }}"></script>--}}
{{--  <script src="{{ asset('dashboard/assets/libs/flot-charts/jquery.flot.tooltip.min.js') }}"></script>--}}
{{--  <script src="{{ asset('dashboard/assets/libs/flot-charts/jquery.flot.selection.js') }}"></script>--}}
{{--  <script src="{{ asset('dashboard/assets/libs/flot-charts/jquery.flot.crosshair.js') }}"></script>--}}

  <!-- Dashboar 1 init js-->
{{--  <script src="{{ asset('dashboard/assets/js/pages/dashboard-1.init.js') }}"></script>--}}

  <!-- App js-->
{{--  <script src="{{asset('dashboard/assets/js/app.min.js')}}"></script>--}}
@yield('after-script')
</body>

</html>
