@extends('backend.layouts.master')
@section('title')
    ' saleOff'
@endsection
@section('after-script')
    <script>
        $(function () {
            $('.country-select').on('change', function () {
                var country = $(".country-select option:selected").attr('value');
                var url = '{{ route("admin.sale")}}';
                window.location.href = url + '?country=' + country;
            })
        })
    </script>
@endsection
@section('contents')
    <h3>saleOff</h3>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title">Edit SaleOff</h4>
                            <div class="row">
                                <form action="{{ route('admin.sale.edit') }}" method="POST">
                                    @csrf
                                    <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label for="simpleinput">Title IOS</label>
                                            <input name="title_ios" type="text" id="simpleinput" class="form-control"
                                                   value="{{ (!empty($data)) ? $data->title_ios : '' }}">
                                            @if($errors->has('title_ios'))
                                                   <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('title_ios')}}</span>
                                            @endif
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="example-email">Title Android</label>
                                            <input type="text" id="example-email" name="title_android"
                                                   class="form-control"
                                                   value="{{ (!empty($data)) ? $data->title_android : '' }}">
                                            @if($errors->has('title_android'))
                                                   <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('title_android')}}</span>
                                            @endif
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="example-password">Description IOS</label>
                                            <input name="description_ios" type="text" id="example-password"
                                                   class="form-control"
                                                   value="{{ (!empty($data)) ? $data->description_ios : ''}}">
                                            @if($errors->has('description_ios'))
                                                   <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('description_ios')}}</span>
                                            @endif
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="example-palaceholder">Description Android</label>
                                            <input name="description_android" type="text" id="example-palaceholder"
                                                   class="form-control"
                                                   value="{{ (!empty($data)) ? $data->description_android : ''}}">
                                            @if($errors->has('description_android'))
                                                   <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('description_android')}}</span>
                                            @endif
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="example-palaceholder">Link IOS</label>
                                            <input name="link_ios" type="text" id="example-palaceholder"
                                                   class="form-control"
                                                   value="{{ (!empty($data)) ? $data->link_ios : ''}}">
                                            @if($errors->has('link_ios'))
                                                   <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('link_ios')}}</span>
                                            @endif
                                        </div>

                                        <div class="form-group mb-3">
                                            <label for="example-palaceholder">Link Android</label>
                                            <input type="text" id="example-palaceholder" class="form-control"
                                                   name="link_android"
                                                   value="{{ (!empty($data)) ? $data->link_android : ''}}">
                                            @if($errors->has('link_android'))
                                                   <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('link_android')}}</span>
                                            @endif
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <h4 class=" offset-md-4 header-title">Sale Android</h4>
                                                <div class="form-group row mb-3">
                                                    <label for="inputPassword3" class="col-3 col-form-label">6
                                                        Month</label>
                                                    <div class="col-9">
                                                        @if(!empty($data->sale_android))

                                                        <input type="number" class="form-control" id="inputPassword3"
                                                               value="{{ (isset($data->sale_android[0]->premium)) ? $data->sale_android[0]->percent : '0' }}"
                                                               name="pre-6-android">
                                                               @if($errors->has('pre-6-android'))
                                                                    <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('pre-6-android')}}</span>
                                                                @endif
                                                            @else
                                                            <input type="number" class="form-control" id="inputPassword3"
                                                                   value="0"
                                                                   name="pre-6-android">
                                                            @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-3">
                                                    <label for="inputPassword5"
                                                           class="col-3 col-form-label">Forever</label>
                                                    <div class="col-9">
                                                        @if(!empty($data->sale_android))
                                                        <input type="number" class="form-control" id="inputPassword5"
                                                               value="{{  (isset($data->sale_android[1]->premium)) ? $data->sale_android[1]->percent : '0' }}"
                                                               name="pre-fore-android">
                                                               @if($errors->has('pre-fore-android'))
                                                                    <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('pre-fore-android')}}</span>
                                                                @endif
                                                            @else
                                                            <input type="number" class="form-control" id="inputPassword5"
                                                                   value="0" name="pre-fore-android">
                                                            @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <!--IOS -->
                                            <div class="col-md-6">
                                                <h4 class=" offset-md-4 header-title">Sale IOS</h4>
                                                <div class="form-group row mb-3">
                                                    <label for="inputPassword3" class="col-3 col-form-label">6
                                                        Month</label>
                                                    <div class="col-9">
                                                        @if(!empty($data->sale_ios))
                                                            <input type="number" class="form-control" name="pre-6-ios"
                                                                   id="inputPassword3"
                                                                   value="{{ ($data->sale_ios[0]->premium == 'pre6months') ? $data->sale_ios[0]->percent : '0' }}">
                                                                @if($errors->has('pre-6-ios'))
                                                                   <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('pre-6-ios')}}</span>
                                                               @endif
                                                        @else
                                                            <input type="number" class="form-control" name="pre-6-ios"
                                                                   id="inputPassword3" value="0">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group row mb-3">
                                                    <label for="inputPassword5"
                                                           class="col-3 col-form-label">Forever</label>
                                                    <div class="col-9">
                                                        @if(!empty($data->sale_ios))
                                                            <input type="number" class="form-control"
                                                                   id="inputPassword5" placeholder="Forever"
                                                                   value="{{  (isset($data->sale_ios[1]->premium)) ? $data->sale_ios[1]->percent : '0' }}" name="pre-fore-ios">
                                                                @if($errors->has('pre-fore-ios'))
                                                                   <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('pre-fore-ios')}}</span>
                                                               @endif
                                                        @else
                                                            <input type="number" class="form-control"
                                                                   id="inputPassword5" name="pre-fore-ios">
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-6">
                                                <b class=" offset-md-4 example-date"><label
                                                        for=" offset-md-4 example-date">Start Android</label></b>
                                                <input class="form-control" id="example-date" type="datetime-local" name="start_android" value="{{ (!empty($data)) ? $data->start_android : ''}}" >
                                                @if($errors->has('start_android'))
                                                    <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('start_android')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <b class=" offset-md-4 example-date"><label
                                                        for=" offset-md-4 example-date">End Android</label></b>
                                                <input class="form-control" id="example-date" type="datetime-local" name="end_android" value="{{ (!empty($data)) ? $data->end_android : ''}}">
                                                @if($errors->has('end_android'))
                                                    <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('end_android')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <b class=" offset-md-4 example-date"><label
                                                        for=" offset-md-4 example-date">Start IOS</label></b>
                                                <input class="form-control" id="example-date" type="datetime-local" name="start_ios" value="{{ (!empty($data)) ? $data->start_ios : ''}}">
                                                @if($errors->has('start_ios'))
                                                    <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('start_ios')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <b class=" offset-md-4 example-date"><label
                                                        for=" offset-md-4 example-date">End IOS</label></b>
                                                <input class="form-control" id="example-date" type="datetime-local" name="end_ios" value="{{ (!empty($data)) ? $data->end_ios : ''}}">
                                                @if($errors->has('end_ios'))
                                                    <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('end_ios')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="simpleinput">Button IOS</label>
                                            <input type="text" id="simpleinput" class="form-control" name="button_ios"  value="{{ (!empty($data)) ? $data->button_ios : ''}}" >
                                            @if($errors->has('button_ios'))
                                                <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('button_ios')}}</span>
                                            @endif
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="simpleinput">Button Android</label>
                                            <input type="text" id="simpleinput" class="form-control" name="button_android" value="{{ (!empty($data)) ? $data->button_android : ''}}">
                                            @if($errors->has('button_android'))
                                                <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('button_android')}}</span>
                                            @endif
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="simpleinput">Cooldown Android</label>
                                            <input type="text" id="simpleinput" class="form-control" name="cooldown_android"  value="{{ (!empty($data)) ? $data->cooldown_android : ''}}">
                                            @if($errors->has('cooldown_android'))
                                                <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('cooldown_android')}}</span>
                                            @endif
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="simpleinput">Cooldown IOS</label>
                                            <input type="text" id="simpleinput" class="form-control" name="cooldown_ios" value="{{ (!empty($data)) ? $data->cooldown_ios : ''}}">
                                            @if($errors->has('cooldown_ios'))
                                                <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('cooldown_ios')}}</span>
                                            @endif
                                        </div>
                                        @if(!empty($data))
                                            <input type="radio" class="input-active active" {{ ($data->active == 1) ? 'checked' : '' }} name="active" value="1"> Active<br>
                                            <input type="radio" class="input-active deactive" {{ ($data->active == 0) ? 'checked' : '' }} name="active" value="0"> Deactive<br>
                                        @else
                                            <input type="radio" class="input-active active"  name="active" value="1"> Active
                                            <br>
                                            <input type="radio" class="input-active deactive"  name="active" value="0"> Deactive<br>
                                        @endif
                                        <button type="submit"
                                                class=" col-3 offset-md-5 btn btn-primary waves-effect waves-light">edit
                                        </button>

                                </div>
                                <div class="col-lg-6">
                                        <div class="form-group mb-3">
                                            <label for="example-select">Quốc gia</label>
                                            <select class="form-control country-select" id="example-select" name="bonus">
                                                @foreach($list as $key => $value)
                                                    <option
                                                        value="{{ $value->country }}" {{ ($country == $value->country) ? 'selected' : '' }}>{{ $value->country }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group mb-3">
                                            <label for="example-palaceholder">thêm quốc gia</label>
                                            <input type="text" id="example-palaceholder" class="form-control"
                                                   placeholder="tên quốc gia" name="country">
                                            @if($errors->has('country'))
                                                   <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('country')}}</span>
                                            @endif
                                        </div>
                                        <button type="submit"
                                                class=" col-3 offset-md-5 btn btn-primary waves-effect waves-light">thêm
                                        </button>
                                </div>
                                <!-- end col -->
                                </form>
                            </div> <!-- end col -->

                        </div> <!-- end row-->
                    </div> <!-- end card-box-->
                </div> <!-- end col-->
            </div><!-- end row -->

        </div> <!-- end container -->
    </div>
@endsection
