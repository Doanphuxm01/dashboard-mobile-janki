@extends('backend.layouts.master')
@section('title')
    report
@endsection
@section('contents')
@include('backend.dashboard.includes.search')
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Tables</h1>
        <!-- DataTales Example -->
        <a href="#" class="btn btn-success btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-check"></i>
                    </span>
            <span class="text">mới nhất</span>
        </a>
{{--        --}}
        <a href="#" class="btn btn-warning btn-icon-split">
                    <span class="icon text-white-50">
                      <i class="fas fa-exclamation-triangle"></i>
                    </span>
            <span class="text">đã sửa</span>
        </a>
        <div class="my-2"></div>
        <div class="my-2"></div>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">DataTables Reports_Janki</h6>
            </div>
            <div class="row">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>stt</th>
                                <th>nội dung lỗi </th>
                                <th>trạng thái </th>
                                <th>ngày báo lỗi</th>
                                <th>biên tập</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($report as $k=>$v)
                                <tr>
                                    <td>{{ $k++ }}</td>
                                    <td>{{ $v->content }}</td>
                                    <td>{{ $v->status }}</td>
                                    <td>{{ $v->created_at->format('d-m-Y') }}</td>
                                    <td>
                                        <div class="checkbox checkbox-success">
                                            <input id="checkbox3" type="checkbox">
                                            <label for="checkbox3">
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @include('backend.dashboard.includes.pagination', ['data' => $report, 'appended' => ['search' => Request::get('search')]])
                </div>
            </div>
        </div>
      
@endsection
